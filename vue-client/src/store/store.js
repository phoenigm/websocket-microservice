import Vue from 'vue'
import Vuex from 'vuex'
import auth from '../store/auth'
import account from '../store/account'
import message from '../store/message'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        account,
        message
    }
});
