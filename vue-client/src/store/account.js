import accountResource from "../api/account";

const account = {
    state: {
        account: {}
    },

    mutations: {
        updateAccount(state, account) {
            state.account = account
        }
    },

    actions: {
        async fetchMyAccount({commit}) {
            const response = await accountResource.getMy();
            commit('updateAccount', response.data);
        },

        async signUp({}, formDetails) {
            try {
                await accountResource.create(formDetails);
            } catch (error) {
                throw error;
            }
        }
    }
};

export default account;
