import messageResource from "../api/message";

const message = {
    state: {
        rooms: [],
        room: {}
    },

    mutations: {
        addMessage(state, message) {
            const roomId = message.roomId;
            console.log("ROOM ID: " + roomId);
            console.log(state.rooms);
            state.rooms.forEach(room => {
                if (room.id === roomId) {
                    room.messages.push(message)
                }
            });
        },
        addRoom(state, room) {
            room.messages = [];
            state.rooms.push(room)
        },
        updateRooms(state, rooms) {
            state.rooms = rooms
        },
        updateRoom(state, roomUpdated) {
            state.room = roomUpdated
        },
        updateMessages(state, messages, roomId) {
            state.rooms.forEach(room => {
                if (room.id === roomId) {
                    room.messages.concat(message)
                }
            });
        }
    },

    actions: {
        async createRoom({commit}, room) {
            const response = await messageResource.createRoom(room);
            commit('addRoom', response.data);
        },

        async fetchMessages({commit}) {
            const response = await messageResource.getMessages();
            commit('updateMessages', response.data);
        },
        async fetchMessagesByRoomId({commit}, roomId) {
            const response = await messageResource.getMessagesByRoomId(roomId);
            commit('updateMessages', response.data, roomId);
        },
        async fetchRooms({commit}) {
            const response = await messageResource.getRooms();
            commit('updateRooms', response.data);
        },

        async fetchRoom({commit}, id) {
            const response = await messageResource.getRoom(id);
            commit('updateRoom', response.data);
        },
    }
};

export default message;
