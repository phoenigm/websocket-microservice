import authResource from "../api/auth";

const auth = {
    state: {
        authDetails: {},
    },

    mutations: {
        updateAuthDetails(state, authDetails) {
            state.authDetails = authDetails
        },

        setAccessToken(state, accessToken) {
            localStorage.setItem('access_token', accessToken);
        },

        setRefreshToken(state, refreshToken) {
            localStorage.setItem('refresh_token', refreshToken);
        },

        deleteAccessToken() {
            localStorage.removeItem('access_token');
        },

        deleteRefreshToken() {
            localStorage.removeItem('refresh_token');
        },

        clearAuthDetails(state) {
            state.authDetails = {};
        }
    },

    actions: {
        logout({commit}) {
            commit('clearAuthDetails');
            commit('deleteAccessToken');
            commit('deleteRefreshToken');
        },

        async refreshToken({commit}) {
            const refreshToken = await localStorage.getItem('refresh_token');
            const response = await authResource.refreshToken(refreshToken);

            commit('updateAuthDetails', response.data);
            commit('setAccessToken', response.data['access_token']);
            commit('setRefreshToken', response.data['refresh_token']);
        },

        async signIn({commit}, formDetails) {
            try {
                const response = await authResource.signIn(formDetails);

                commit('updateAuthDetails', response.data);
                commit('setAccessToken', response.data['access_token']);
                commit('setRefreshToken', response.data['refresh_token']);
                return response;
            } catch (error) {
                throw error;
            }
        }
    }
};

export default auth;
