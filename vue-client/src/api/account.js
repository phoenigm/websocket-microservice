import axios from '../api/axios.config'
import store from '../store/store'
import router from '../router/router'

const accountApi = axios.create({
    baseURL: 'http://localhost:9000/accounts'
});

accountApi.interceptors.request.use(
    config => {
        const accessToken = localStorage.getItem('access_token');

        if (accessToken) {
            config.headers.authorization = 'Bearer ' + accessToken;
        }
        return config;
    }, error => error
);

accountApi.interceptors.response.use(
    response => response,
    error => {
        if (error.response.status === 401) {
            store.dispatch('refreshToken')
                .then(response => {
                    router.go(0);
                });
        }
        return Promise.reject(error);
    }
);

export default {
    get: id => accountApi.get(`/${id}`),
    getMy: () => accountApi.get('/my'),
    create: accountDetails => accountApi.post('', accountDetails),
    update: account => account.put(`/${id}`, account),
    delete: id => accountApi.delete(`/${id}`),
    getAll: () => accountApi.get('')
}