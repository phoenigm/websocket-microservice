import axios from '../api/axios.config'

const authApi = axios.create({
    baseURL: 'http://localhost:9000/',
});

export default {
    signIn: details => {
        const authDetailsForm = new FormData();
        authDetailsForm.append('grant_type', 'password');
        authDetailsForm.append('client_id', 'public');
        authDetailsForm.append('username', details.email);
        authDetailsForm.append('password', details.password);
        return authApi.post('/oauth/token', authDetailsForm);
    },
    refreshToken: token => {
        const authDetailsForm = new FormData();
        authDetailsForm.append('grant_type', 'refresh_token');
        authDetailsForm.append('client_id', 'public');
        authDetailsForm.append('refresh_token', token);
        return authApi.post('/oauth/token', authDetailsForm);
    }
}