import axios from '../api/axios.config'

const messagesApi = axios.create({
    baseURL: 'http://localhost:9000/messages'
});

export default {
    createRoom: room => messagesApi.post('/rooms', room),
    getMessages: () => messagesApi.get(''),
    getMessagesByRoomId: (roomId) => messagesApi.get('/'+ roomId),
    getRooms: () => messagesApi.get('/rooms'),
    getRoom: (id) => messagesApi.get('/rooms/' + id),
}