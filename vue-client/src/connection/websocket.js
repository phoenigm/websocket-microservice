import SockJS from 'sockjs-client'
import { Stomp } from '@stomp/stompjs'

let stompClient = null;
let handlerFunction = null;

export function connect() {
    stompClient = Stomp.over(new SockJS('http://localhost:9003/room/chat'));
    stompClient.connect({}, frame => {
        console.log('Connect frame: ' + frame);
        stompClient.subscribe('/topic/chat', message => {
            handlerFunction(message.body)
        })
    });
}

export function sendMessage(message) {
    console.log("Sent message: ");
    console.log(JSON.stringify(message));
    stompClient.send("/app/send", {}, JSON.stringify(message))
}

export function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect()
    }
}

export function addHandler(handler) {
    handlerFunction = handler
}
