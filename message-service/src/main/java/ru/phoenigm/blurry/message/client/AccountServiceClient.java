package ru.phoenigm.blurry.message.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.phoenigm.blurry.message.domain.AccountDetails;

@FeignClient(name = "account-service",
        configuration = AccountServiceClientConfiguration.class,
        fallback = AccountServiceClient.AccountServiceClientFallback.class)
public interface AccountServiceClient {

    @GetMapping(value = "/accounts/{id}")
    AccountDetails getAccountDetails(@PathVariable("id") Long id);

    @Component
    class AccountServiceClientFallback implements AccountServiceClient {
        @Override
        public AccountDetails getAccountDetails(Long id) {
            return null;
        }
    }
}