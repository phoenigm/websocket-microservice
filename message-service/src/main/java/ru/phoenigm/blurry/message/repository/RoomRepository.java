package ru.phoenigm.blurry.message.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.phoenigm.blurry.message.domain.Room;

public interface RoomRepository extends JpaRepository<Room, Long> {
}
