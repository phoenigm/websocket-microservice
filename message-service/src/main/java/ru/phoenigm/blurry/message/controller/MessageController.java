package ru.phoenigm.blurry.message.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import ru.phoenigm.blurry.message.domain.Message;
import ru.phoenigm.blurry.message.domain.Room;
import ru.phoenigm.blurry.message.service.MessageService;

import java.util.List;

@RequestMapping("/messages")
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping
    public List<Message> messages() {
        return messageService.getMessages();
    }

    @GetMapping("/{id}")
    public List<Message> messagesInRoom(@PathVariable Long id) {
        return messageService.getMessagesByRoomId(id);
    }

    @GetMapping("/rooms/{id}")
    public Room room(@PathVariable Long id) {
        return messageService.getRoom(id);
    }

    @GetMapping("/rooms")
    public List<Room> rooms() {
        return messageService.getRooms();
    }

    @PostMapping("/rooms")
    public Room createRoom(@RequestBody RoomRequest room) {
        return messageService.createRoom(Room.builder()
                .name(room.name)
                .build()
        );
    }


    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Data
    public static class RoomRequest {
        private String name;
    }
}
