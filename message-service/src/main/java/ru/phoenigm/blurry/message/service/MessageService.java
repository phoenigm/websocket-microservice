package ru.phoenigm.blurry.message.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.phoenigm.blurry.message.client.AccountServiceClient;
import ru.phoenigm.blurry.message.domain.AccountDetails;
import ru.phoenigm.blurry.message.domain.Message;
import ru.phoenigm.blurry.message.domain.Room;
import ru.phoenigm.blurry.message.repository.MessageRepository;
import ru.phoenigm.blurry.message.repository.RoomRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private AccountServiceClient accountServiceClient;

    public List<Message> getMessages() {
        return messageRepository.findAll();
    }

    public List<Message> getMessagesByRoomId(Long id) {
        return messageRepository.findMessagesByRoomId(id);
    }

    public List<Room> getRooms() {
        return roomRepository.findAll();
    }

    public Room createRoom(Room room) {
        room.setAvatar("https://avatars.dicebear.com/v2/gridy/" + room.getName() + ".svg");
        return roomRepository.save(room);
    }

    public Message saveMessage(Message message) {
        AccountDetails account = accountServiceClient.getAccountDetails(message.getAuthor().getId());
        message.setAuthor(account);
        message.setSentDate(LocalDateTime.now());
        return messageRepository.save(message);
    }

    public Room getRoom(Long id) {
        return roomRepository.findById(id).get();
    }
}
