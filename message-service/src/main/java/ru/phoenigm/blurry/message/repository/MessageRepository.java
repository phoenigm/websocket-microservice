package ru.phoenigm.blurry.message.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.phoenigm.blurry.message.domain.Message;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findMessagesByRoomId(Long id);
}
