package ru.phoenigm.blurry.message.controller;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import ru.phoenigm.blurry.message.domain.AccountDetails;
import ru.phoenigm.blurry.message.domain.Message;
import ru.phoenigm.blurry.message.service.MessageService;

@Controller
public class ChatController {

    @Autowired
    private MessageService messageService;

    @SendTo("/topic/chat")
    @MessageMapping("/send")
    public Message processMessage(MessageRequest message) {
        return messageService.saveMessage(
                Message.builder()
                        .message(message.message)
                        .author(AccountDetails.builder().id(message.authorId).build())
                        .roomId(message.roomId)
                        .build()
        );
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Data
    public static class MessageRequest {
        private String message;
        private Long roomId;
        private Long authorId;
    }
}
