package ru.phoenigm.blurry.message.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Embeddable
public class AccountDetails {
    private Long id;
    private String firstName;
    private String lastName;
    private String avatarUrl;
}
