package ru.phoenigm.blurry.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@EnableAuthorizationServer
@Configuration
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    @Value("${project.jwt.signingKey}")
    private String signingKey;
    @Value("${project.client.auth-service.secret}")
    private String authServiceClientSecret;
    @Value("${project.client.account-service.secret}")
    private String accountServiceClientSecret;
    @Value("${project.client.message-service.secret}")
    private String messageServiceClientSecret;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtAccessTokenConverter accessTokenConverter;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private DefaultTokenServices tokenServices;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter tokenConverter = new JwtAccessTokenConverter();
        tokenConverter.setSigningKey(signingKey);
        return tokenConverter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(this.accessTokenConverter);
    }

    @Bean
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(this.tokenStore);
        tokenServices.setTokenEnhancer(this.accessTokenConverter);
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setAccessTokenValiditySeconds(60 * 30);
        tokenServices.setRefreshTokenValiditySeconds(60 * 60 * 24 * 30 * 2);
        return tokenServices;
    }

    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        // @formatter:off
        endpoints
                .authenticationManager(this.authenticationManager)
                .tokenServices(this.tokenServices);
        // @formatter:on
    }

    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        // @formatter:off
        clients
                .inMemory()
                    .withClient("public")
                    .authorizedGrantTypes("password", "refresh_token")
                    .scopes("ui")
                    .and()
                .withClient("account-service")
                    .secret(this.passwordEncoder.encode(accountServiceClientSecret))
                    .authorizedGrantTypes("client_credentials", "refresh_token")
                    .scopes("server")
                    .and()
                .withClient("message-service")
                    .secret(this.passwordEncoder.encode(messageServiceClientSecret))
                    .authorizedGrantTypes("client_credentials")
                    .scopes("server")
                    .and()
                .withClient("auth-service")
                    .secret(this.passwordEncoder.encode(authServiceClientSecret))
                    .authorizedGrantTypes("client_credentials")
                    .scopes("server");
        // @formatter:on
    }

    public void configure(AuthorizationServerSecurityConfigurer security) {
        // @formatter:off
        security
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()")
                .passwordEncoder(this.passwordEncoder)
                .allowFormAuthenticationForClients();
        // @formatter:on
    }
}
