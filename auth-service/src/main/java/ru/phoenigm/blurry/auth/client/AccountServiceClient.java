package ru.phoenigm.blurry.auth.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.phoenigm.blurry.auth.security.UserCredentials;

@FeignClient(name = "account-service",
        configuration = AccountServiceClientConfiguration.class,
        fallback = AccountServiceClient.AccountServiceClientFallback.class)
public interface AccountServiceClient {

    @GetMapping(value = "/accounts")
    UserCredentials getUserCredentials(@RequestParam("email") String email);

    @Component
    class AccountServiceClientFallback implements AccountServiceClient {
        @Override
        public UserCredentials getUserCredentials(String email) {
            return null;
        }
    }
}
