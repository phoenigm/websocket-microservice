package ru.phoenigm.blurry.auth.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.phoenigm.blurry.auth.client.AccountServiceClient;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private AccountServiceClient accountServiceClient;

    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserCredentials userCredentials = accountServiceClient.getUserCredentials(email);

        if (userCredentials == null) {
            throw new UsernameNotFoundException("Not found username with email " + email);
        }

        return userCredentials;
    }
}
