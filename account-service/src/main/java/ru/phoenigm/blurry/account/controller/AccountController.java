package ru.phoenigm.blurry.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.phoenigm.blurry.account.domain.Account;
import ru.phoenigm.blurry.account.service.AccountService;

import java.security.Principal;
import java.util.List;

@RequestMapping("/accounts")
@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PreAuthorize("#oauth2.hasScope('server')")
    @GetMapping
    public Account accountByEmail(@RequestParam String email) {
        return accountService.getAccountByEmail(email);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/my")
    public Account myAccount(Principal principal) {
        return accountService.getAccountByEmail(principal.getName());
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/{id}")
    public Account accountById(@PathVariable Long id) {
        return accountService.getAccount(id);
    }

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/all")
    public List<Account> accounts() {
        return accountService.getAccounts();
    }

    @PostMapping
    public void createAccount(@RequestBody Account account) {
        accountService.createAccount(account);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/{id}")
    public void updateAccount(@PathVariable Long id, @RequestBody Account account) {
        account.setId(id);
        accountService.updateAccount(account);
    }

    @PreAuthorize("isAuthenticated()")
    @PutMapping
    public void updateMyAccount(Principal principal) {
        Account account = accountService.getAccountByEmail(principal.getName());
        accountService.updateAccount(account);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public void deleteAccount(@PathVariable Long id) {
        accountService.deleteAccount(id);
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping
    public void deleteMyAccount(Principal principal) {
        Account account = accountService.getAccountByEmail(principal.getName());
        accountService.deleteAccount(account.getId());
    }
}
