package ru.phoenigm.blurry.account.service;

import ru.phoenigm.blurry.account.domain.Account;

import java.util.List;

public interface AccountService {
    Account createAccount(Account account);

    Account updateAccount(Account account);

    void deleteAccount(Long id);

    Account getAccount(Long id);

    Account getAccountByEmail(String email);

    List<Account> getAccounts();
}
