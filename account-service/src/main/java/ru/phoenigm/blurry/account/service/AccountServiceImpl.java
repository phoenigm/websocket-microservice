package ru.phoenigm.blurry.account.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.phoenigm.blurry.account.domain.Account;
import ru.phoenigm.blurry.account.exception.AccountAlreadyExistsException;
import ru.phoenigm.blurry.account.exception.AccountNotFoundException;
import ru.phoenigm.blurry.account.repository.AccountRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Account createAccount(Account account) {
        Optional<Account> accountWithSameEmail = accountRepository.findByEmail(account.getEmail());

        if (accountWithSameEmail.isPresent()) {
            throw new AccountAlreadyExistsException("Account with email " + account.getEmail() + " already exists");
        }

        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setAvatarUrl(fetchAvatarUrl(account.getEmail()));
        account.setAuthorities(Set.of("USER"));
        account.setRegistrationDate(LocalDateTime.now());
        return accountRepository.save(account);
    }

    @Override
    public List<Account> getAccounts() {
        return accountRepository.findAll();
    }

    @Override
    public Account getAccount(Long id) {
        return accountRepository.findById(id)
                .orElseThrow(() -> new AccountNotFoundException("Not found account with id " + id));
    }

    @Override
    public Account getAccountByEmail(String email) {
        return accountRepository.findByEmail(email)
                .orElseThrow(() -> new AccountNotFoundException("Not found account with email " + email));
    }

    @Override
    public Account updateAccount(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public void deleteAccount(Long id) {
        accountRepository.deleteById(id);
    }

    private  String fetchAvatarUrl(String username) {
        return "https://api.adorable.io/avatars/285/" + username + ".png";
    }
}
