package ru.phoenigm.blurry.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.phoenigm.blurry.account.domain.Account;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByEmail(String email);
}
