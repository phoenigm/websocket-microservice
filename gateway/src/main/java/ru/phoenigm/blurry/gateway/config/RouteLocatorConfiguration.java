package ru.phoenigm.blurry.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteLocatorConfiguration {
    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p -> p
                        .path("/messages/**", "/room/**")
                        .uri("lb://message-service")
                )
                .route(p -> p
                        .path("/ws/room/**")
                        .uri("lb://message-service")
                )
                .route(p -> p
                        .path("/oauth/**")
                        .uri("lb://auth-service")
                )
                .route(p -> p
                        .path("/accounts/**")
                        .uri("lb://account-service")
                )
                .build();
    }
}