# Spring Boot Microservice Architecture

## Run in production mode:
>
`$ docker-compose up -d`
>

## Run in development mode:
>
`$ docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d`
>
